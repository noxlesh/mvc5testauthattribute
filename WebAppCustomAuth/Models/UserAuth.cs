namespace WebAppCustomAuth.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class CustomUserInitializer : CreateDatabaseIfNotExists<CustomUserContext>
    {
        protected override void Seed(CustomUserContext context)
        {
            context.CustomUsers.Add(new CustomUser {UserName = "admin", Password = "12345"});
        }
    }

    public class CustomUserContext : DbContext
    {
        public CustomUserContext(): base("Conn")
        {
            Database.SetInitializer(new CustomUserInitializer());
        }

        public virtual DbSet<CustomUser> CustomUsers { get; set; }
    }

    public class CustomUser
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}