﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using WebAppCustomAuth.Models;

namespace WebAppCustomAuth.Controllers
{

    class MyAuthAttribute : FilterAttribute, IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            HttpCookie userCookie = filterContext.HttpContext.Request.Cookies["session"];
            if (userCookie == null)
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            HttpCookie userCookie = filterContext.HttpContext.Request.Cookies["session"];
            if (userCookie == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new System.Web.Routing.RouteValueDictionary {
                        { "controller", "Main" }, { "action", "Auth" }
                    });
            }
        }
    }

    public class MainController : Controller
    {
        CustomUserContext db = new CustomUserContext();

        [MyAuth]
        // GET: Main
        public ActionResult Index()
        {
            
            return View();
        }

        [HttpGet]
        public ActionResult Auth()
        {
            HttpCookie userCookie = Request.Cookies["session"];
            if (userCookie != null)
            {
                string user = userCookie["user"];
                if (db.CustomUsers.Where(u => u.UserName == user).Any())
                    ViewBag.Auth = true;
                    return View();
            }

            ViewBag.Auth = false;
            return View();
        }

        [HttpPost]
        public ActionResult Auth(string user, string pass)
        {
            if (db.CustomUsers.Where(u => u.UserName == user && u.Password == pass).Any())
            {
                HttpCookie userCookie = new HttpCookie("session");
                userCookie["user"] = user;
                userCookie.Expires = DateTime.Now.AddHours(1);
                Response.Cookies.Add(userCookie);
                Response.Flush();
                ViewBag.Auth = true;
                return View();
            }

            return Redirect("Auth");
        }
    }
}